using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class moveGameObjectCamera : MonoBehaviour
{
    public GameObject camera;

    private void Update()
    {
        transform.position = camera.transform.position;
        transform.rotation = camera.transform.rotation;
    }


}
