using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MoverMundo : MonoBehaviour
{
    public Transform camera, mundo;
    public Vector3 intencidadDesplazamiento;
    Vector2 startTouchPosition, endTouchPosition;
    public Vector3 distanceAdvance;
    public Text text;

    IEnumerator animacion;
    void Update()
    {
        CheckTypeSwipe();
    }
    private void CorrerAnimacion()
    {
        if (animacion != null)
        {
            StopCoroutine(animacion);
            animacion = AnimacionMover(true);
            StartCoroutine(animacion);            
        }
        else
        {
            animacion = AnimacionMover(true);
            StartCoroutine(animacion);
        }
    }
    IEnumerator AnimacionMover(bool forward)
    {
        text.text = "inicio";
        float duracion = 2, tiempo = 0, lerp = 0;
        Vector3 posicionFinal = forward ? mundo.position - camera.forward : mundo.position + camera.forward; 
        while(tiempo < duracion)
        {
            lerp = tiempo / duracion;
            tiempo += Time.deltaTime;
            mundo.position = Vector3.Lerp(mundo.position, posicionFinal, lerp);
            yield return null;
        }
        yield return null;
    }
    public void CheckTypeSwipe()
    {
        if (Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Began)
        {
            startTouchPosition = Input.GetTouch(0).position;
        }
        if (Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Ended)
        {
            endTouchPosition = Input.GetTouch(0).position;
            if (endTouchPosition.y > startTouchPosition.y)
            {
                text.text = "avanza";
                CorrerAnimacion();
            }
            
        }
    }
}
